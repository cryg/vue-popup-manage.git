<!--
 * @Author: jidongxiao
 * @Date: 2020-06-15 15:18:58
 * @LastEditors: jidongxiao
 * @LastEditTime: 2020-11-04 18:21:50
 * @Description: ***
-->
# vue-popup-manage
弹窗组件状态管理和组件传值
## Installation
```
$ yarn add vue-popup-manage
$ npm install vue-popup-manage
```

### Usage
```
import Popup from 'vue-popup-manage';

const popupPath = 'components';
// Configure the popup path globally, The full path is 'src/components'
Vue.use(Popup, popupPath);

```

### API
##### Show or hide current popup and hide other popup
```
this.$popup.closeToggle({
    NAME: 'popup',  // 弹窗组件的文件名称
    SHOW: true, // true 显示弹窗组件,false 隐藏弹窗组件
    SELECTED: iconName, // 页面图标选中
    // PATH:'components', // 可单独配置 弹窗组件的文件路径，默认用全局配置的路径
    data: item.data,   // 弹窗组件传值
});
```
##### Show or hide current popup
```
this.$popup.toggle({
    NAME: 'popup',  // 弹窗组件的文件名称
    SHOW: true, // true 显示弹窗组件,false 隐藏弹窗组件
    SELECTED: iconName, // 页面图标选中
    // PATH:'components', // 可单独配置 弹窗组件的文件路径，默认用全局配置的路径
    data: item.data,   // 弹窗组件传值
});
```
##### Hide popups
```
// popup1  弹窗组件的文件名称
// 一个参数
this.$popup.close('popup1');
// 多个参数
this.$popup.close(['popup1','popup2'...]);
```
##### Hide all popups exclude popup1...
```
// popup1  弹窗组件的文件名称
// 不传参数
this.$popup.closeAll()
// 一个参数
this.$popup.closeAll('popup1');
// 多个参数
this.$popup.closeAll(['popup1','popup2'...]);
```
##### Icon selected status
```
// iconName 图标的key 对应弹窗弹出配置的 SELECTED: iconName,
this.$popup.selected('iconName');
// html 中的用法
<div :class="{'active': $popup.selected('iconName')}"></div>
```
##### Component received value
```
// 组件中监听值的变化 this.popupKey  组件的名称  对应动态组件的  :popup-key
this.$popup.data(this.popupKey)
// 返回值的类型是对象包含弹窗弹出配置的所有值
```

##### The currently loaded component
```
// 当前页面中弹出的所有组件
this.$popup.components()
// html 用法  循环所有组件
<component :is="componentItem.component" :popup-key="componentItem.name" :popup-index="index" v-for="componentItem in $popup.components()" :key="componentItem.name"></component>
```
### Demo
1.引入组件 参考：Usage

2.在需要显示弹窗的主页面例如(App.vue)，创建动态组件模板  参考 The currently loaded component

3.调用弹窗的api 显示或者隐藏弹窗
### Repositories
https://gitee.com/cryg/vue-popup-manage.git



