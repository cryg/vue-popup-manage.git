import Vue from 'vue';

declare module 'vue/types/vue' {
  interface Vue {
    $popup: any;
    $popupPath: any;
  }
}
