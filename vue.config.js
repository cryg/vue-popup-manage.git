// const CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = {
  lintOnSave: false,
  productionSourceMap: false,
  pages: {
    index: {
      entry: ['./src/lib/index.ts'],
      filename: 'vue-popup-manage.js',
      library: 'vue-popup-manage',   //reqire引入的名字
      libraryTarget: 'umd',
      umdNamedDefine: true,
      title: 'vue-popup-manage',
    },
    test: {
      entry: 'src/main.ts',
      template: 'public/index.html',
    },
  },
  chainWebpack: (config) => {
    // config.plugin('copy').use(CopyWebpackPlugin,[{
    //   patterns: [
    //     { from: 'src/lib/', to: 'lib/' },
    //   ],
    // }]);
  }
}
