import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    dialog: {
      // show:'', // 弹窗显示隐藏
      // selected:'', // 页面选中
      // action:[], // 选中图标的方法
      // data:'' // 数据
      // components:'' // 组件
    },
  },
  getters: {
    // 判断页面图标是否选中
    getSelected: (state: any) => (val: any) => {
      if (val || val > -1) {
        for (const name  in state.dialog) {
          if (state.dialog[name].selected === val) {
            return true;
          }
        }
      } else {
        return false;
      }
    },
    // 根据dialog的名称返回 dialog的值
    getDialogByName: (state: any) => (val: any) => {
      if (state.dialog[val]) {
        return state.dialog[val].data;
      } else {
        return '';
      }
    },
    // 获取当前加载的所有组件
    getDialogComponents: (state: any) => {
      if (state.dialog) {
        const components: any = [];
        for (const name  in state.dialog) {
          if (state.dialog[name].components) {
            components.push(state.dialog[name].components);
          }
        }
        return  components;
      }
    },
    // 返回页面当前选中的action
    getSelectedActions: (state: any) => (val: any) => {
      if (state.dialog) {
        const actions: any = [];
        for (const name  in state.dialog) {
          if (state.dialog[name].action) {
            actions.push(state.dialog[name].action);
          }
        }
        return  actions;
      }
    },
  },
  mutations: {
    // 初始化页面的弹窗
    initPageDialog: (state: any, data: any) => {
      state.dialog = {};
      state.dialog = {...state.dialog};
    },
    // 关闭所有弹窗 指定的除外
    closeAllDialog: (state: any, exclude?: any) => {
      for (const name  in state.dialog) {
        if (exclude && (name === exclude || exclude.includes(name))) {
          return;
        } else {
          delete state.dialog[name];
        }
      }
      state.dialog = {...state.dialog};
    },
    // 关闭指定名称的弹窗
    closeDialog: (state: any, nameArray: any) => {
      let deleteStatus: boolean = false;
      if (typeof nameArray === 'string') {
        deleteStatus = true;
        delete state.dialog[nameArray];
      } else {
        nameArray.forEach((name: any) => {
          if (state.dialog.hasOwnProperty(name)) {
            deleteStatus = true;
            delete state.dialog[name];
          }
        });
      }
      if (deleteStatus) {
        state.dialog = {...state.dialog};
      }
    },
    // 切换弹窗状态
    toggleDialog: (state: any, data: any) => {
      if (data.NAME) {
        if (!state.dialog.hasOwnProperty(data.NAME)) {
          // 动态加载组件
          state.dialog[data.NAME] = {};
          if (data.NAME !== 'STORES_ELECTED') {
            const popupComponent: any = () => import(`@/${data.PATH}/${data.PROPUP}.vue`);
            state.dialog[data.NAME].components = {
              name: data.NAME,
              component: popupComponent,
            };
          }
        }
        state.dialog[data.NAME].show = data.SHOW;
        if (data.SHOW) {
          if (data.hasOwnProperty('SELECTED')) {
            state.dialog[data.NAME].selected = data.SELECTED;
          }
          if (data.hasOwnProperty('ACTION')) {
            state.dialog[data.NAME].action = data.ACTION;
          }
          state.dialog[data.NAME].data = data;
          state.dialog = {...state.dialog};
        } else {
          // 删除对象
          delete state.dialog[data.NAME];
          state.dialog = {...state.dialog};
        }
      }
    },
  },
  actions: {
    // 关闭其他弹窗切换弹窗状态
    closeAndToggleDialog({ commit }, data) {
      if (data.SHOW) {
        // 显示弹窗 关闭其他弹窗
        commit('closeAllDialog', data.NAME);
      }
      commit('toggleDialog', data);
    },
    // 切换当前弹窗状态
    toggleDialog({ commit }, data) {
      commit('toggleDialog', data);
    },

  },
});

