import Vue from 'vue';
import App from './App.vue';
import store from './lib/store';
import Popup from './lib';
const popupPath = 'components';

Vue.use(Popup, popupPath);

Vue.config.productionTip = false;
new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
