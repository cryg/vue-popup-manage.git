import store from './store';
const Popup: any = {};
Popup.install =  (Vue: any, options: any) => {
    Vue.prototype.$popup =  {
           // 加载组件html 模板

           // 互斥显示隐藏弹窗
            closeToggle: (data: any) => {
                if (!data.hasOwnProperty('PATH')) {
                  data.PATH = options;
                }
                data.PROPUP = data.NAME;
                data.NAME = `${data.NAME}_${data.SELECTED}`;
                store.dispatch('closeAndToggleDialog', data);
            },
            // 显示隐藏弹窗
            toggle: (data: any) => {
                if (!data.hasOwnProperty('PATH')) {
                    data.PATH = options;
                }
                data.PROPUP = data.NAME;
                data.NAME = `${data.NAME}_${data.SELECTED}`;
                store.dispatch('toggleDialog', data);
            },
            // 关闭指定名称弹窗
            close: (data: any) => {
                store.commit('closeDialog', data);
            },
            // 关闭所有弹窗 指定的除外
            closeAll: (data?: any) => {
                store.commit('closeAllDialog', data);
            },
            // 初始化页面弹窗
            init: () => {
                store.commit('initPageDialog');
            },
            // 图标是否选中
            selected: (data: any) => {
                return  store.getters.getSelected(data);
            },
            // 弹窗传值
            data: (data: any) => {
                return  store.getters.getDialogByName(data);
            },
            // 组件加载
            components: () => {
               return  store.getters.getDialogComponents;
            },
            // 方法重新加载
            actions: () => {
                return store.getters.getSelectedActions;
            },
    };
};
export default  Popup;
